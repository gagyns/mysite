<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/','prviController@init');

Route::get('/', [
    'uses' => 'prviController@index',
    'as' => 'main.index'
]);

Route::get('/porfolio/{id}', [
    'uses' => 'prviController@enterPortFolio',
    'as' => 'main.enterPortfolio'
]);

Route::get('/slideshow/{portfolioType}', [
    'uses' => 'prviController@portfolioslideShow',
    'as' => 'main.slideshow'
]);

Route::get('/about', [
    'uses' => 'prviController@about',
    'as' => 'main.about'
]);
Route::get('/contact', [
    'uses' => 'prviController@contact',
    'as' => 'main.contact'
]);

Route::get('/dbtest', [
    'uses' => 'prviController@dbtest',
    'as' => 'main.dbtest'
]);

Route::post('/postmail', [
    'uses' => 'prviController@contactPost',
    'as' => 'main.postmail'
]);

Route::get('/myadmin', [
    'uses' => 'AdminController@enterAdminGround',
    'as' => 'admin.ground'
]);

Route::post('/myadminCategorySelect', [
    'uses' => 'AdminController@selectCategory',
    'as' => 'admin.setVisible'
]);

Route::get('/myadminedit', [
    'uses' => 'AdminController@enterEditContent',
    'as' => 'admin.editContent'
]);

Route::get('/myadminAdd', [
    'uses' => 'AdminController@enterAddContent',
    'as' => 'admin.addContent'
]);


Route::post('/myadminPostNewContent', [
    'uses' => 'AdminController@postNewContent',
    'as' => 'admin.postContent'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Application Routes
    |--------------------------------------------------------------------------
    |
    | This route group applies the "web" middleware group to every route
    | it contains. The "web" middleware group is defined in your HTTP
    | kernel and includes session state, CSRF protection, and more.
    |
    */
    
Route::group(['middleware' => ['web']], function () {
    Route::get('/myadminDelete/{id}/{portfolioID}', [
        'uses' => 'AdminController@deleteImage',
        'as' => 'edit.deleteImage'
    ]);
});
