<div 
  class="js-main-page-header @if(!isset($porfolioKey))main-page-header header-block-brp1000px @else main-page-header-portfolio @endif"
>
<!--   @if(!isset($porfolioKey))
    <img 
      src="public/images/avatar500px.jpg" 
      alt="Smiley face" 
      class="main-page-avatar header-avatar-brp1000px"
    >
 @endif -->
  <div class="main-page-welcome-word welcome-text-brp1000px">
    <p class="welcome-word">
      Dragan Živanović :: {{$navigateFlag}} :: {{{ isset($porfolioKey) ? $porfolioKey : '' }}}
    </p>
  </div>
</div>