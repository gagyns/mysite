<div class="main-page-futer ">
  <a class="futer-text-style @if($navigateFlag == 'portfolio') link-active @endif" 
     href="{{ route('main.index') }}">
    <p>
      portfolio :: 
    </p>
  </a>
  <a class="futer-text-style @if($navigateFlag == 'about') link-active @endif" 
     href="{{ route('main.about') }}">
    <p>
      about me ::
    </p>
  </a>
  <a class="futer-text-style @if($navigateFlag == 'contact') link-active @endif" 
     href="{{ route('main.contact') }}">
    <p>
      :: contact
    </p>
  </a>
</div>

