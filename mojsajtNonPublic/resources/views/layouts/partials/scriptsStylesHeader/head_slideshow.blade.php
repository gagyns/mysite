<title>Portfolio Gallery: {{$portfolioType -> portfolio}}</title>

<meta http-equiv="content-type" content="text/html; charset=UTF-8" />

<link rel="stylesheet" href="{{ URL::asset('css/ssGallery/supersized.css')}}" type="text/css" media="screen" />

<link rel="stylesheet" href="{{ URL::asset('css/ssGallery/supersized.shutter.css')}}" type="text/css" media="screen" />

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<script type="text/javascript" src="{{ URL::asset('vendorJS/ssGallery/jquery.easing.min.js')}}"></script>

<script type="text/javascript" src="{{ URL::asset('vendorJS/ssGallery/supersized.3.2.7.min.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('vendorJS/ssGallery/supersized.shutter.min.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />