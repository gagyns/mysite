<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{ URL::asset('vendorCSS/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('vendorCSS/material.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<script type="text/javascript" src="{{ URL::asset('vendorJS/jquery-3.2.1.min.js') }}"></script>
<script src="{{ URL::asset('vendorJS/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('vendorJS/material.min.js') }}"></script>
<script src="{{ URL::asset('js/app.js') }}"></script>
