  @include('layouts.partials.common.header')
    @if(count($errors) > 0)
      <div class="alert alert-danger">
          @foreach($errors->all() as $error)
              <p>{{ $error }}</p>
          @endforeach
      </div>
  @endif
  @if ($jobSegment == 'prepare')
   <!-- Simple Textfield -->
    <div class="u_margin-auto u_width-50pct mail-box">
      <form action="{{ route('main.postmail') }}" method="POST">
        {{ csrf_field() }}
        <div class="mdl-textfield mdl-js-textfield u_width-100pct">
          <input class="mdl-textfield__input" name="sendermail" type="text" id="sender_email">
          <label class="mdl-textfield__label" >Sender e-mail adress</label>
        </div>
        <div class="mdl-textfield mdl-js-textfield u_width-100pct">
          <input class="mdl-textfield__input" name="subject" type="text" id="subject">
          <label class="mdl-textfield__label" for="sample1">Subject</label>
        </div>
        <div class="mdl-textfield mdl-js-textfield u_width-100pct">
          <textarea class="mdl-textfield__input" name="mailcontent" type="text" rows= "4" id="mailcontent" ></textarea>
          <label class="mdl-textfield__label" for="mailcontent">Mail text.</label>
        </div>
        <!-- Flat button with ripple -->
        <button class="mdl-button mdl-js-button mdl-js-ripple-effect"  type="submit" >
          Send
        </button>
                <!-- Flat button with ripple -->
        <button class="mdl-button mdl-js-button mdl-js-ripple-effect"  type="reset" >
          Reset
        </button>
      </form>
    </div>
  @elseif ($jobSegment == 'send')
  <div>
    <p class="u_margin-auto u_width-50pct"> Mesage sent </p>    
  </div>
  @endif
  <div class="u_margin-auto u_width-50pct mail-box">
    <h1> Contact information: </h1>
    <hr>
    <table class="table_style">
      <tr>
        <td>Name and surname: </td>
        <td>Dragan Zivanovic</td>
      </tr>
      <tr>
        <td>Adress:</td>
        <td>Milutina Bojica 27</td>
      </tr>
      <tr>
        <td>Telephone:</td>
        <td>060/57-38-60</td>
      </tr>
      <tr>
        <td>email:</td>
        <td>gagyns@gmail.com</td>
      </tr>
    </table>

    <div id="map"></div>
    <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfM3cW-4O8OHWlMsfmT_Swmb2Rw1mqB2Y&callback=initMap">
    </script>
    <script>
      function initMap() {
        var uluru = {lat: -25.363, lng: 131.044};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>



  </div>
  @include('layouts.partials..common.futer') 