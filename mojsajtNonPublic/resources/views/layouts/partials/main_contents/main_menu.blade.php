<div class="main-manu-container-brp1000px main_content-container js-scrol-detect">
  @foreach ($menuItems as $keyChunk=>$menuItem)
    @if ($menuItem->visible === 1)
      <div class="chunk chunkNum{{$keyChunk}}">
        <div class="u_float-left main-manu-item-container main-manu-item-container-brp1000px main-manu-item-container-brp500px">
          <a href="{{ route('main.enterPortfolio', ['id' => $keyChunk]) }}">
            <img 
              src="images/main_meny_images/{{$menuItem -> image}}"
              alt="{{$menuItem -> image}}" 
              class="main-mani-images"
            >
            <div class="main-manu-item-label">
                <p class="u_color-black main_meny_label_text">{{ $menuItem -> label}} </p>
            </div>
          </a>
        </div> 
      </div>
    @endif
  @endforeach
  <div class="hidenlink">
    <a href="{{ route('admin.ground')}}">
      myadmin
    </a>
  </div>
</div>
