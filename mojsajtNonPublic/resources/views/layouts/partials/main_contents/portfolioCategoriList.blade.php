<div class='main_content-container-list-images js-scrol-detect'>
@foreach ($imagesData as $key=>$imagesData)
  <div class="portfolio_item-container">
    @if($imagesData->type === 'movie')
    <video width="60%" height="100%" controls>
      <source
      src="{{ URL::asset('images/portfolio_movies/'.$imagesData->imageName) }}" 
      type="video/mp4">
    </video>
    @else
      <a href="{{ route('main.slideshow', ['porfolioKey' => $porfolioKey]) }}">
      <img 
        src="{{ URL::asset('images/portfolio_images/LR_images/'.$imagesData->imageName) }}" 
        alt="{{$imagesData->lrimage}}" 
        class="portfolio_item-image @if ($key % 2 == 0)u_float-left @else u_float-right @endif"
      >
      @endif
      <div class="imgInfoText @if ($key % 2 == 0)u_float-left @else u_float-right @endif">
        <div class="portfolio_about_header">
          <p>
            {{$imagesData->description}}
          </p>  
        </div>
        <div class="portfolio_about_description">
          <p>
            {{$imagesData->about}}
          </p>
        </div>
      </div>
      <div class="clear-fix"></div>
    </a>
<!--     <div class="clear-fix clear-fix-brp1000px"> </div> -->
  </div>
@endforeach
</div>