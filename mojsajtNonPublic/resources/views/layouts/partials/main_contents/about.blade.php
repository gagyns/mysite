<div class="main_content-container">
  <div class="about_avatar"> </div>
  <div class="about_me_text u_about_margin_left">
    <!-- <p>ABOUT ME ::</p> -->
    {!! $articles[0]->content !!}
  </div>
  <br>  
  <div class="u_main-line-text u_margin-bottom10px u_about_margin_left u_brand-container-width">
    Tehnologies
  </div>
  <div class="about_brand-list-container u_about_margin_left u_brand-container-width u_overflow-hedden">
    <div class="onetehnology onetehnology-brp1000px">
      <img src="{{ URL::asset('images/about/3dmax.jpg') }}" alt="logo3dmax" class="brand-logo">
      {!! $articles[1]->content !!}
    </div>
    <div class="onetehnology onetehnology-brp1000px">
      <img src="{{ URL::asset('images/about/zbrush.png') }}" alt="logozbrush" class="brand-logo">
      {!! $articles[2]->content !!}
    </div>
    <div class="onetehnology onetehnology-brp1000px">
      <img src="{{ URL::asset('images/about/premiere.jpg') }}" alt="premiereLogo" class="brand-logo">
      <img src="{{ URL::asset('images/about/after.png') }}" alt="afterEfect" class="brand-logo">
      <img src="{{ URL::asset('images/about/vegas.jpeg') }}" alt="Vegas" class="brand-logo">
      {!! $articles[3]->content !!}
    </div>
    <div class="onetehnology onetehnology-brp1000px">
      <img src="{{ URL::asset('images/about/photoshop.png') }}" alt="Photoshop" class="brand-logo">
      <img src="{{ URL::asset('images/about/adobeIlustrator.png') }}" alt="iluustrator" class="brand-logo">
      <img src="{{ URL::asset('images/about/corel.png') }}" alt="corel" class="brand-logo">
      {!! $articles[4]->content !!}
    </div>
    <i class="clear-fix"></i>
  </div>
  <div class="about_brand-list-container u_about_margin_left u_brand-container-width u_margin-top15pcx">
    <div class="web_tehnologies-logos">
      <img src="{{ URL::asset('images/about/html-css-js-php-mysql.png') }}" alt="logo3dmax" class="brand-logo-web u_width16perc">
      <img src="{{ URL::asset('images/about/react.png') }}" alt="logo3dmax" class="brand-logo-web">
      <img src="{{ URL::asset('images/about/d3js.jpg') }}" alt="logo3dmax" class="brand-logo-web">
      <img src="{{ URL::asset('images/about/laravel.png') }}" alt="logo3dmax" class="brand-logo-web">
      <img src="{{ URL::asset('images/about/spring.png') }}" alt="logo3dmax" class="brand-logo-web">
      {!! $articles[5]->content !!}
    </div>
 
    <i class="clear-fix"></i>
  </div>
</div>