<div class="devPortfolio_mainContent">
  @foreach ($links as $link)
    <div class="devPortfolio_linkImage">
      <a href="{{ $link->link }}">
        <img 
          src="../devPortfolio/welcome_screen_assets/{{$link->picture}}.jpg"
          alt="{{ $link->picture}}" 
          class="u_height_100perc u_width-100pct"
        >
      </a>
    </div>
  @endforeach
  <a href="https://www.globexpo.rs">
    <div class="devPortfolio_linkImage">
      <img class="u_height_100perc u_width-100pct" src="../devPortfolio/welcome_screen_assets/globexpo.jpg" alt="globexpo site"> 
      <p class= "label"> Simple PHP site for local company </p>
    </div>
  </a>
</div>