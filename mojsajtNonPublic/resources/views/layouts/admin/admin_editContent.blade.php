<div>
  {{ Session::get('key') }}
</div>
@foreach ($imagesData as $imageData)
<div class="myadmin_edit_category_container">
  <div class="myadmin_heder" attrID="{{$imageData->id}}">
    <p class="myadmin_header-label">{{ $imageData->portfolio}}</p>
  <div class="close js-myadmin_toggle" attrID="{{$imageData->id}}">
    <svg class= "arrow-down_{{$imageData->id}}" style="width:24px;height:24px" viewBox="0 0 24 24">
      <path fill="#000000" d="M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z" />
    </svg>
    <svg class= "arrow-up_{{$imageData->id}} dislplayNone" style="width:24px;height:24px" viewBox="0 0 24 24">
      <path fill="#000000" d="M7.41,15.41L12,10.83L16.59,15.41L18,14L12,8L6,14L7.41,15.41Z" />
    </svg>
  </div>
  </div>
<div class="myadmin__section @if (Session::get('key') == $imageData->id)myadmin__section--is-open @endif" 
  id="js-section_{{$imageData->id}}"
>
    @foreach ($imageData->images as $image)
    <div class="myadmin_oneitem_for_edit">
      <img 
        src="{{ URL::asset('images/portfolio_images/LR_images/'.$image->imageName) }}" 
        alt="{{$image->imageName}}" 
        class="portfolio_item-image"
      >
      <div class="side_information padding-all">
        <p class="u_font-color_white">Image name: {{ $image->imageName }}</p><hr>
        <p class="u_font-color_white">Image description: {{ $image->description }}</p>
      </div>
      <div class="delete-icon" attrID="{{$image->id}}">
        <a href="{{ route('edit.deleteImage', ['id' => $image->id, 'portfolioID'=>$imageData->id]) }}">
          <svg style="width:24px;height:24px" viewBox="0 0 24 24"
            class="absolute_null ">
            <path
              class="delete-icon-svg delete-icon-svg_{{$image->id}}"
              d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z" />
          </svg>
          <svg style="width:24px;height:24px" viewBox="0 0 24 24"
            class="absolute_null ">
            <path 
              class="delete-icon-open-svg_{{$image->id}} dislplayNone u_fill-red"
              d="M20.37,8.91L19.37,10.64L7.24,3.64L8.24,1.91L11.28,3.66L12.64,3.29L16.97,5.79L17.34,7.16L20.37,8.91M6,19V7H11.07L18,11V19A2,2 0 0,1 16,21H8A2,2 0 0,1 6,19Z" />
          `</svg>
        </a>
      </div>
    </div>
    @endforeach
  </div>

</div>
    
@endforeach