<div class="addContent">
  <div class="u_margin-auto u_width-50pct mail-box">
  @if($errors->any())
    <h1 style="color:red">{{$errors->first()}}</h1>
    <br>
  @endif
    <form action="{{ route('admin.postContent') }}" method="POST"
    enctype="multipart/form-data">
      {{ csrf_field() }}
      <div>
        <p>Select portfolio where to put image</p>
        <select name="portfolios">
        @foreach ($portfolio_kategories as $category)
          <option value="{{$category->id}}">{{$category->portfolio}}</option>
        @endforeach
        </select>
      </div>
      <br>
      <div>
        <input
          name="imageName" 
          type="text" 
          id="imageName"
          placeholder="Image name"
        >
      </div>
      <br>
      <div>
        <textarea placeholder="Write something about image" 
          name="aboutImage" cols="40" rows="5"></textarea>
      </div>
      <br>
      <input name="myFile" type="file">
      <button class="mdl-button mdl-js-button mdl-js-ripple-effect"  type="submit" >
        Send
      </button>
    </form>
    <hr>
    
  </div>
</div>