<!DOCTYPE html>
<html>
    <head>
        <title>Dragan Zivanovic :: MyAdmin</title>
        @include('layouts.partials.scriptsStylesHeader.head')
    </head>
    <body>
      @include('layouts.partials.common.header')
      @if ($selectedAction == 'ground')
        <div>
          <a href="{{ route('admin.editContent') }}">
            EDIT CONTENT
          </a>
        </div>
        <div>
          <a href="{{ route('admin.addContent') }}">
            ADD CONTENT
          </a>
        </div>
        <div class="view_category-switch-container u_margin_all_20">
          <form action="{{ route('admin.setVisible') }}" method="POST">
            {{ csrf_field() }}
            @foreach($portfolio_categories as $category)
              <div>
                <input type="checkbox" 
                  id="portfolio_{{$category->id}}" 
                  name="visible_portfolio[]" 
                  value="{{$category->id}}"
                  class="mdl-icon-toggle__input"
                  {{$category->visible==1 ? 'checked' : ''}}
                >
                <label for="portfolio_{{$category->id}}">
                  {{$category->label}}
                </label>
              </div>
            @endforeach
            <div class="u_margin_all_20">
              <button type="submit">Subscribe</button>
            </div>
          </form>
        </div>
      @elseif ($selectedAction == 'editContent')
        <div class="main_content-container-list-images js-scrol-detect">
          @include('layouts.admin.admin_editContent')
        </div>
      @elseif ($selectedAction == 'addContent')
        <div class="main_content-container-list-images js-scrol-detect">
          @include('layouts.admin.admin_addContent')
        </div>
      @endif
      @include('layouts.partials.common.futer')
    </body>
</html>