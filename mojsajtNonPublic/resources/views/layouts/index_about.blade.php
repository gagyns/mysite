<!DOCTYPE html>
<html>
    <head>
        <title>Dragan Zivanovic :: About</title>
        @include('layouts.partials.scriptsStylesHeader.head')
    </head>
    <body>
        @include('layouts.partials.content_about')
    </body>
</html>
