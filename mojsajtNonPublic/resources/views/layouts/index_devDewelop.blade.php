<!DOCTYPE html>
<html>
    <head>
        <title>Dragan Zivanovic :: Portfolio</title>
        @include('layouts.partials.scriptsStylesHeader.head')
    </head>
    <body>
        @include('layouts.partials.contentDevPortfolio')
    </body>
</html>