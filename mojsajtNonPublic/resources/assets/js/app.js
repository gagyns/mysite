$( document ).ready(function() {
  $('.js-scrol-detect').on('scroll', function() {
    if($(this).scrollTop() + $(this).innerHeight() + 20 >= $(this)[0].scrollHeight) {
      console.log('end reached')
    }
    if($(this).scrollTop() <= 20) {
      console.log('top reached')
    }
  })
  $('.myadmin_heder').click(function() {
    var containerID =  $(this).attr('attrID');
    $( '#js-section_' + containerID)
    .toggleClass( 'myadmin__section--is-open' );
    $('.arrow-down_' + containerID).toggleClass( 'dislplayNone' );
    $('.arrow-up_' + containerID).toggleClass( 'dislplayNone' );
  })
  
  $('.delete-icon').mouseover(function() {
    var containerID =  $(this).attr('attrID');
    $('.delete-icon-svg_' + containerID).toggleClass( 'dislplayNone' );
    $('.delete-icon-open-svg_' + containerID).toggleClass( 'dislplayNone' );
  })
  $('.delete-icon').mouseout(function() {
    var containerID =  $(this).attr('attrID');
    $('.delete-icon-svg_' + containerID).toggleClass( 'dislplayNone' );
    $('.delete-icon-open-svg_' + containerID).toggleClass( 'dislplayNone' );
  })
});