<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    public function portfolio()
  {
    return $this->belongsTo('App\Portfolio', 'portfolios_id');
  }
}
