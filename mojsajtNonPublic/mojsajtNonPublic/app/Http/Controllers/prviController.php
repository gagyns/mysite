<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\DBtest;
use App\menyItem;
use App\Portfolio;
use App\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Mail;

class prviController extends Controller
{
  public function index() {
    $meniModel = new menyItem;
    $menyItems = $meniModel -> get();
    return view('layouts.index', ['navigateFlag' => 'portfolio', 'menuItems' => $menyItems]);
  }

  public function enterPortFolio(Request $request, $id) {
    // because forEch begin from 0 but portfolios_id start from 1
    $indexForImages = $id + 1;
    $imagesData = Portfolio::find($indexForImages)->images()->get();
    $portFolio = Portfolio::where('id', $indexForImages)->get();

    return view('layouts.index_portfolioList', [
      'navigateFlag' => 'portfolio', 
      'porfolioKey' => $portFolio[0] -> portfolio, 
      'imagesData' => $imagesData
    ]);
  }
  
  public function portfolioslideShow(Request $request, $portfolioType){
    $portFolio = Portfolio::where('portfolio', $portfolioType)->get();       
    $imagesData = Portfolio::find($portFolio[0]->id)->images()->get();
 
    $imageDataArray = $imagesData->map(function ($d) { 
      $name = $d -> imageName;
      $description = $d -> description;
      $title = 'Titl ubaciti u bazu';
      $ingInformation = Array('name' => $name, 'description' => $description, 'title' => $title);
      return $ingInformation;
    });
    $imageDataArrayForJS = $imageDataArray->toArray();
    return view('layouts.index_slideshow', ['imageDataArrayForJS' => $imageDataArrayForJS, 'portfolioType' => $portFolio[0]]); 
  }
  
  public function about() {
    return view('layouts.index', ['navigateFlag' => 'about']);
  }
  
  public function contact() {
    return view('layouts.index_contact', [
      'porfolioKey' => 'information',
      'navigateFlag' => 'contact',
      'jobSegment' => 'prepare'
    ]);
  }

  public function contactPost(Request $request) {
    $this->validate($request, [
      'sendermail' => 'required|email',
      'subject' => 'required|max:120|min:4',
      'mailcontent' => 'required|max:120|min:4'
    ]);

    $data = array(
      'email' => $request['sendermail'],
      'subject' => $request['subject'],
      'bodyMessage' => $request['mailcontent']
    );

    Mail::send('email_form.email', $data, function($message) use ($data){
      $message->from($data['email']);
      $message->to('hello@devmarketer.io');
      $message->subject($data['subject']);
    });

    return view('layouts.index_contact', [
      'navigateFlag' => 'contact',
      'jobSegment' => 'send'
    ]);
  }

  public function index2() {
  $users = DB::table('DBtests2')->get();
//       return view('welcome', ['users' => $users]);

  return Response::json(array('data' => $users));
   }
}
