<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    //
  public $timestamps = false;
  protected $fillable = [ 'visible' ];
  public function images()
  {
    return $this->hasMany('App\ImageDB', 'portfolios_id');
  }
}
