<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\DB;
use App\ImageDB;
use App\Imageabout;
use App\Portfolio;
use App\menyItem;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{
    public function enterAdminGround() {
        $portfolioCategories = menyItem::get();
        return view('layouts.admin.admin_ground',
        ['navigateFlag' => 'Admin page',
        'selectedAction' => 'ground',
        'portfolio_categories' => $portfolioCategories]);
    }

    public function selectCategory(Request $request) {
      $selectedCategories = $request['visible_portfolio'];
      if (empty($selectedCategories)) {
        dd('at least one item must be selected');
      };
      $affected = DB::table('menyitems')->update(array('visible' => 0));
      foreach($selectedCategories as $value) {
        $selectedPortfolio = menyItem::find($value);
        $selectedPortfolio->visible = 1;
        $selectedPortfolio->save();
      }
      return Redirect::back();
    }

    public function enterEditContent(Request $request) {
        // on first load set open segment to null
        $hasSession = $request->session()->has('key');
        if(!$hasSession) {
            $request->session()->put('key', 0);
        } 
        $picForEdit = Portfolio::with('Images')->get();
        return view('layouts.admin.admin_ground',
        ['navigateFlag' => 'Admin page',
        'selectedAction' => 'editContent',
        'imagesData' => $picForEdit]);
    }

    public function enterAddContent() {
        $porfoliosCategories = Portfolio::get();
        // here is procedure for saving in related tables
        // $image = new Image;
        // $data = ['imageName' => 'imeSlike1',
        //     'description' => 'dfg'];
        // $image->fill($data);
        // $image->portfolios_id = 2;
        // $image->save();
        // $dataAbout = new Imageabout;
        // $dataAbout->description = 'aprilili';
        // $image->imageabout()->save($dataAbout);
        return view('layouts.admin.admin_ground',
        ['navigateFlag' => 'Admin page',
        'selectedAction' => 'addContent',
        'portfolio_kategories' => $porfoliosCategories]);
    }

    public function deleteImage(Request $request, $id, $portfolioID) {
        $hasSession = $request->session()->has('key');
        if($hasSession) {
            $request->session()->forget('key');
        } 
        $request->session()->put('key', $portfolioID);
        $imageToDelete = ImageDB::find($id);
        $imageToDelete->delete();
        return Redirect::back();
    }

    public function postNewContent(Request $request) {
        if($request->hasFile('myFile')){
            // get file name and clean whitespaces
            $fileName = $request['imageName'];
            $fileNameWithoutSpace = preg_replace('/\s+/', '_', $fileName);
            
            $portfolioId = $request['portfolios'];
            $imageAbout = $request['aboutImage'];
            $request->file('myFile')
              ->storeAs('images/portfolio_images/HR_images/', $fileNameWithoutSpace.'.jpg'  ,'public');

            // open an image file AND RESIZE
            $imgSmall = Image::make('images/portfolio_images/HR_images/'.$fileNameWithoutSpace.'.jpg')
              ->resize(640, 480);

            // finally we save the image as a new file
            $imgSmall->save('images/portfolio_images/LR_images/'.$fileNameWithoutSpace.'.jpg');
            // --------
            $calbackVariable = $this->saveImageToDB($fileNameWithoutSpace, $portfolioId, $imageAbout);
            return $calbackVariable ;
        }
        return Redirect::back()->withErrors(['Please select image file from your local drive']);
    }

    private function saveImageToDB($fileNameWithoutSpace, $portfolioId, $imageAbout){
      $imageDBmodel = new imageDB;

      $data = ['imageName' => $fileNameWithoutSpace.'.jpg',
          'description' => $imageAbout, 'portfolios_id' => $portfolioId];
      $imageDBmodel->fill($data);
      $imageDBmodel->save();
      $dataAbout = new Imageabout;
      $dataAbout->description = $imageAbout;
      $imageDBmodel->imageabout()->save($dataAbout);
    //   return ">> portFolioId".$portfolioId."::filename: "
    //     .$fileNameWithoutSpace.":: about image".$imageAbout;
    return redirect()->action(
      'prviController@enterPortFolio', ['id' => $portfolioId - 1]
    );

    }
}
