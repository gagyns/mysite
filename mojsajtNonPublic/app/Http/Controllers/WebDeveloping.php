<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WebDevPortolio;

class WebDeveloping extends Controller
{
    //
    public function webDevPortfolio() {
        $WebDevPortolioModel = new WebDevPortolio;
        $linkImageArr = $WebDevPortolioModel -> get();
        return view('layouts.index_devDewelop', [
            'links' => $linkImageArr, 'navigateFlag' => 'portfolio', 
            'porfolioKey' => 'Data vizualization'
            ]);
    }

    public function enterDevPortfolio(Request $request, $id, $description) {
        // dd($description);
        return view('layouts.index_devDewelop_enterItem', [
            'id' => $id,
            'navigateFlag' => $description
        ]);
    }
}
