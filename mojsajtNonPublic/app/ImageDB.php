<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageDB extends Model
{
  protected $table = 'images';
  protected $fillable = [
    'imageName',
    'description',
    'portfolios_id'
  ];
  public $timestamps = false;
    //
  public function portfolio() {
      return $this->belongsTo('App\Portfolio', 'portfolios_id');
  }

  public function imageabout() {
    return $this->hasOne('App\Imageabout', 'image_id');
  }
}
