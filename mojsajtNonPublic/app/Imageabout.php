<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imageabout extends Model
{
    protected $fillable = ['description'];
    public $timestamps = false;

    public function images()
    {
      return $this->belongsTo('App\ImageDB', 'image_id');
    }
}
