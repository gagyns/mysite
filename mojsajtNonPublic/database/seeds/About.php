<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class About extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('abouts')->delete();
        $articles = [
            ['article' => 'rec_o_meni', 'content' => '<h1 class="u_main-line-text">About me ::</h1>
            <br>
            <p>
                  Hello, <br/>
                  my name is Dragan, I am a graphic engineer and I am engaged in animation and 3D graphics since 2001.<br/>
              During this period, I had the opportunity to work in many areas of: <br/>
              <ul>
                <li>architecture, </li>
                <li>advertising, </li>
                <li>character animation,</li>
                <li>making a character for video games.</li> 
              </ul><br/>
              Some of the works that are not covered by the contract are shown in my portfolio.<br/>
        
              Dedicated to the tasks and deadlines of delivery, I have successfully collaborated with 
              individuals and companies mostly as freelancers.<br/>
        
              If you have any questions or suggestions for cooperation, I look forward to contacting and
               discussing with you.
            </p>'],
            ['article' => '3dmax', 'content' => '<p>3d studio max is the main program for 3D visualization and animation I work with. <br/>
              As a rendering engine, I use the V-ray and mental ray. <br/>
              For the Animation character I work with the CAT tool while the other specificities in animations<br/>
              I use add-ons that provide the maximum score at that moment on the market.
            </p>'],
            ['article' => 'zbrush', 'content' => '<p>
              Z-brush is leading program in the field of digital clay modeling <br/>
              I use this program as a modeling character, <br/>
              whether it is the use of video production or games. <br/>
              In all areas, this program provides maximum quality.
            </p>'],
            ['article' => 'videoEdit', 'content' => '<p>Adobe premiere, after effect, Soni vegas pro are the leading software packages for video 
              production and postproduction. I actively use these programs when creating animations and video materials.
              I also use these programs when working non-linear video editing and video production.
            </p>'],
            ['article' => 'photoshop', 'content' => '<p>Great experience in the use of tools, who are leaders in the market of graphic tools, active use 
              of Photoshop, Corel and Adobe Illustrator in the web design industry as well as in the preparation 
              for printing and video production.
            </p>'],
            ['article' => 'webDevelop', 'content' => '<p>In web programming, I use web technologies such as HTML, SASS, JavaScript, also for using data 
            visualization and manipulating SVG vector images and graphics using the d3.js library. For Beck end,
             I use PHP with a larval frame and when it comes to JAVA programming language I use spring framework.
            </p>'],
        ];
        DB::table('abouts')->insert($articles);
    }
}
