<?php

use Illuminate\Database\Seeder;

class WebDewPortfolio extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('web_dev_portolios')->delete();
        $linkImageArr = [
            ['link' => 'http://lived3d.net/ReactPlayground/', 'picture' => 'reactPlayground', 'description' => 'ReactJS and d3js complex app'],
            ['link' => 'http://lived3d.net/priority_chart/', 'picture' => 'priorityChart', 'description' => 'Matrix chart with two modes (right icon swich them)'],
            ['link' => 'http://lived3d.net/coverageOptimizer/', 'picture' => 'coverageOptimizer', 'description' => 'ReactJS and d3js complex app'],
            ['link' => 'http://lived3d.net/D3js_basic/', 'picture' => 'd3Logo', 'description' => 'd3js playground and tests'],
            ['link' => 'http://lived3d.net/smartChart/', 'picture' => 'pie_barChart', 'description' => 'Dragable d3js chart. Three modes Half Pie / Pie/ Bar chart'],
            ['link' => 'http://lived3d.net/Sunburst/', 'picture' => 'sunBurst', 'description' => 'd3js radial chart drillDown feature alse bredcrump navigation'],
            ['link' => 'http://lived3d.net/TimeSeriesChart/', 'picture' => 'timeSeries', 'description' => 'Comlex chart with layouts and projetcs nav bars'],
        ];
        DB::table('web_dev_portolios')->insert($linkImageArr);
    }
}
