/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(2);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

$(document).ready(function () {
  $('.js-scrol-detect').on('scroll', function () {
    if ($(this).scrollTop() + $(this).innerHeight() + 20 >= $(this)[0].scrollHeight) {
      console.log('end reached');
    }
    if ($(this).scrollTop() <= 20) {
      console.log('top reached');
    }
  });
  $('.myadmin_heder').click(function () {
    var containerID = $(this).attr('attrID');
    $('#js-section_' + containerID).toggleClass('myadmin__section--is-open');
    $('.arrow-down_' + containerID).toggleClass('dislplayNone');
    $('.arrow-up_' + containerID).toggleClass('dislplayNone');
  });

  $('.delete-icon').mouseover(function () {
    var containerID = $(this).attr('attrID');
    $('.delete-icon-svg_' + containerID).toggleClass('dislplayNone');
    $('.delete-icon-open-svg_' + containerID).toggleClass('dislplayNone');
  });
  $('.delete-icon').mouseout(function () {
    var containerID = $(this).attr('attrID');
    $('.delete-icon-svg_' + containerID).toggleClass('dislplayNone');
    $('.delete-icon-open-svg_' + containerID).toggleClass('dislplayNone');
  });
});

/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgNWUxOWNlNTg1N2JkZDk2MTYyNjgiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9hcHAuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9zYXNzL2FwcC5zY3NzIl0sIm5hbWVzIjpbIiQiLCJkb2N1bWVudCIsInJlYWR5Iiwib24iLCJzY3JvbGxUb3AiLCJpbm5lckhlaWdodCIsInNjcm9sbEhlaWdodCIsImNvbnNvbGUiLCJsb2ciLCJjbGljayIsImNvbnRhaW5lcklEIiwiYXR0ciIsInRvZ2dsZUNsYXNzIiwibW91c2VvdmVyIiwibW91c2VvdXQiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7O0FDN0RBQSxFQUFHQyxRQUFILEVBQWNDLEtBQWQsQ0FBb0IsWUFBVztBQUM3QkYsSUFBRSxrQkFBRixFQUFzQkcsRUFBdEIsQ0FBeUIsUUFBekIsRUFBbUMsWUFBVztBQUM1QyxRQUFHSCxFQUFFLElBQUYsRUFBUUksU0FBUixLQUFzQkosRUFBRSxJQUFGLEVBQVFLLFdBQVIsRUFBdEIsR0FBOEMsRUFBOUMsSUFBb0RMLEVBQUUsSUFBRixFQUFRLENBQVIsRUFBV00sWUFBbEUsRUFBZ0Y7QUFDOUVDLGNBQVFDLEdBQVIsQ0FBWSxhQUFaO0FBQ0Q7QUFDRCxRQUFHUixFQUFFLElBQUYsRUFBUUksU0FBUixNQUF1QixFQUExQixFQUE4QjtBQUM1QkcsY0FBUUMsR0FBUixDQUFZLGFBQVo7QUFDRDtBQUNGLEdBUEQ7QUFRQVIsSUFBRSxnQkFBRixFQUFvQlMsS0FBcEIsQ0FBMEIsWUFBVztBQUNuQyxRQUFJQyxjQUFlVixFQUFFLElBQUYsRUFBUVcsSUFBUixDQUFhLFFBQWIsQ0FBbkI7QUFDQVgsTUFBRyxpQkFBaUJVLFdBQXBCLEVBQ0NFLFdBREQsQ0FDYywyQkFEZDtBQUVBWixNQUFFLGlCQUFpQlUsV0FBbkIsRUFBZ0NFLFdBQWhDLENBQTZDLGNBQTdDO0FBQ0FaLE1BQUUsZUFBZVUsV0FBakIsRUFBOEJFLFdBQTlCLENBQTJDLGNBQTNDO0FBQ0QsR0FORDs7QUFRQVosSUFBRSxjQUFGLEVBQWtCYSxTQUFsQixDQUE0QixZQUFXO0FBQ3JDLFFBQUlILGNBQWVWLEVBQUUsSUFBRixFQUFRVyxJQUFSLENBQWEsUUFBYixDQUFuQjtBQUNBWCxNQUFFLHNCQUFzQlUsV0FBeEIsRUFBcUNFLFdBQXJDLENBQWtELGNBQWxEO0FBQ0FaLE1BQUUsMkJBQTJCVSxXQUE3QixFQUEwQ0UsV0FBMUMsQ0FBdUQsY0FBdkQ7QUFDRCxHQUpEO0FBS0FaLElBQUUsY0FBRixFQUFrQmMsUUFBbEIsQ0FBMkIsWUFBVztBQUNwQyxRQUFJSixjQUFlVixFQUFFLElBQUYsRUFBUVcsSUFBUixDQUFhLFFBQWIsQ0FBbkI7QUFDQVgsTUFBRSxzQkFBc0JVLFdBQXhCLEVBQXFDRSxXQUFyQyxDQUFrRCxjQUFsRDtBQUNBWixNQUFFLDJCQUEyQlUsV0FBN0IsRUFBMENFLFdBQTFDLENBQXVELGNBQXZEO0FBQ0QsR0FKRDtBQUtELENBM0JELEU7Ozs7OztBQ0FBLHlDIiwiZmlsZSI6Ii9qcy9hcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSAwKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCA1ZTE5Y2U1ODU3YmRkOTYxNjI2OCIsIiQoIGRvY3VtZW50ICkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICQoJy5qcy1zY3JvbC1kZXRlY3QnKS5vbignc2Nyb2xsJywgZnVuY3Rpb24oKSB7XG4gICAgaWYoJCh0aGlzKS5zY3JvbGxUb3AoKSArICQodGhpcykuaW5uZXJIZWlnaHQoKSArIDIwID49ICQodGhpcylbMF0uc2Nyb2xsSGVpZ2h0KSB7XG4gICAgICBjb25zb2xlLmxvZygnZW5kIHJlYWNoZWQnKVxuICAgIH1cbiAgICBpZigkKHRoaXMpLnNjcm9sbFRvcCgpIDw9IDIwKSB7XG4gICAgICBjb25zb2xlLmxvZygndG9wIHJlYWNoZWQnKVxuICAgIH1cbiAgfSlcbiAgJCgnLm15YWRtaW5faGVkZXInKS5jbGljayhmdW5jdGlvbigpIHtcbiAgICB2YXIgY29udGFpbmVySUQgPSAgJCh0aGlzKS5hdHRyKCdhdHRySUQnKTtcbiAgICAkKCAnI2pzLXNlY3Rpb25fJyArIGNvbnRhaW5lcklEKVxuICAgIC50b2dnbGVDbGFzcyggJ215YWRtaW5fX3NlY3Rpb24tLWlzLW9wZW4nICk7XG4gICAgJCgnLmFycm93LWRvd25fJyArIGNvbnRhaW5lcklEKS50b2dnbGVDbGFzcyggJ2Rpc2xwbGF5Tm9uZScgKTtcbiAgICAkKCcuYXJyb3ctdXBfJyArIGNvbnRhaW5lcklEKS50b2dnbGVDbGFzcyggJ2Rpc2xwbGF5Tm9uZScgKTtcbiAgfSlcbiAgXG4gICQoJy5kZWxldGUtaWNvbicpLm1vdXNlb3ZlcihmdW5jdGlvbigpIHtcbiAgICB2YXIgY29udGFpbmVySUQgPSAgJCh0aGlzKS5hdHRyKCdhdHRySUQnKTtcbiAgICAkKCcuZGVsZXRlLWljb24tc3ZnXycgKyBjb250YWluZXJJRCkudG9nZ2xlQ2xhc3MoICdkaXNscGxheU5vbmUnICk7XG4gICAgJCgnLmRlbGV0ZS1pY29uLW9wZW4tc3ZnXycgKyBjb250YWluZXJJRCkudG9nZ2xlQ2xhc3MoICdkaXNscGxheU5vbmUnICk7XG4gIH0pXG4gICQoJy5kZWxldGUtaWNvbicpLm1vdXNlb3V0KGZ1bmN0aW9uKCkge1xuICAgIHZhciBjb250YWluZXJJRCA9ICAkKHRoaXMpLmF0dHIoJ2F0dHJJRCcpO1xuICAgICQoJy5kZWxldGUtaWNvbi1zdmdfJyArIGNvbnRhaW5lcklEKS50b2dnbGVDbGFzcyggJ2Rpc2xwbGF5Tm9uZScgKTtcbiAgICAkKCcuZGVsZXRlLWljb24tb3Blbi1zdmdfJyArIGNvbnRhaW5lcklEKS50b2dnbGVDbGFzcyggJ2Rpc2xwbGF5Tm9uZScgKTtcbiAgfSlcbn0pO1xuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvYXBwLmpzIiwiLy8gcmVtb3ZlZCBieSBleHRyYWN0LXRleHQtd2VicGFjay1wbHVnaW5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvc2Fzcy9hcHAuc2Nzc1xuLy8gbW9kdWxlIGlkID0gMlxuLy8gbW9kdWxlIGNodW5rcyA9IDAiXSwic291cmNlUm9vdCI6IiJ9